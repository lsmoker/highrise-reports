TESTMODE = '' # '' emails to Highrise users, use 'dev1' or 'dev2' for testing

SYSADMIN = ('Sys Admin', 'sysadmin@example.com')

HR_USER1 = ('Highrise User1', 'hruser1@example.com')
HR_USER2 = ('Highrise User2', 'hruser2@example.com')
HR_USER3 = ('Highrise User3', 'hruser3@example.com')
HR_USER4 = ('Highrise User4', 'hruser4@example.com')

if TESTMODE == 'dev1':
    HR_USER1 = ('Highrise User1', 'dev1@example.com')
    HR_USER2 = ('Highrise User2', 'dev1@example.com')
    HR_USER3 = ('Highrise User3', 'dev1@example.com')
    HR_USER4 = ('Highrise User4', 'dev1@example.com')
elif TESTMODE == 'dev2':
    HR_USER1 = ('Highrise User1', 'dev2@example.com')
    HR_USER2 = ('Highrise User2', 'dev2@example.com')
    HR_USER3 = ('Highrise User3', 'dev2@example.com')
    HR_USER4 = ('Highrise User4', 'dev2@example.com')

DAYS_BACK_P = 30
DAYS_BACK_C = 14

REPORTS_NC = [{'title': 'No Contact in {0} Days (User1)'.format(DAYS_BACK_P),
                'alltags': {'User1', 'Active'}, 'excludetag': '', 'to': HR_USER1, 'cc': (HR_USER3, HR_USER4, SYSADMIN)},
              {'title': 'No Contact in {0} Days (User2)'.format(DAYS_BACK_P),
                'alltags': {'User2', 'Active'}, 'excludetag': '', 'to': HR_USER2, 'cc': (HR_USER3, HR_USER4, SYSADMIN)},
              {'title': 'No Contact in {0} Days (User3)'.format(DAYS_BACK_P),
                'alltags': {'User3', 'Active'}, 'excludetag': 'Business', 'to': HR_USER3, 'cc': (HR_USER4, SYSADMIN)}]

REPORTS_NA = [{'title': 'Nobody Assigned', 'notags': ['User1', 'User2', 'User3'],
               'to': (HR_USER1, HR_USER2, HR_USER3), 'cc': (SYSADMIN)},
              {'title': 'No Status Assigned', 'notags': ['Active', 'Closed', 'Business'],
               'to': (HR_USER1, HR_USER3), 'cc': (SYSADMIN)}]

REPORTS_CNC = [{'title': 'No Contact (Cases) in {0} Days'.format(DAYS_BACK_C),
                'to': (HR_USER1, HR_USER2, HR_USER3), 'cc': (SYSADMIN)}]
