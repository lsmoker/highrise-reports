# Highrise CMS API
HIGHRISE_API_KEY = 'api_key'
HIGHRISE_USER = 'api_user'

# AWS SMTP Config
EMAIL_HOST = 'smtp.example.com'
EMAIL_HOST_USER = 'user'
EMAIL_HOST_PASSWORD = 'password'
EMAIL_PORT = 587
EMAIL_FROM = ('User Name', 'user@example.com')

LOCAL_TZ = file("/etc/timezone").read().strip()

PERSON_LINK = '<a href="https://{0}.highrisehq.com/people/{1}" title="Open page in Highrise">{2} {3}</a>'
CASE_LINK = '<a href="https://{0}.highrisehq.com/kases/{1}" title="Open page in Highrise">{2}</a>'

HTML_TABLE = """<HTML>
<body>
    <h1>{0}</h1>
    <table border="1">
        {1}
    </table>
</body>
</HTML>"""

HTML_TR = "<tr>{0}</tr>"
HTML_TD = "<td>{0}</td>"
