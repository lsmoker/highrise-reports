from settings import *

import emails

def send_report(html, subject, mail_from, mail_to, cc=None):
    message = emails.html(html=html,
                          subject=subject,
                          mail_from=mail_from,
                          mail_to=mail_to,
                          cc=cc)
    r = message.send(smtp={'host': EMAIL_HOST, 'port': EMAIL_PORT, 'tls': True,
                           'user': EMAIL_HOST_USER, 'password': EMAIL_HOST_PASSWORD})
    assert r.status_code == 250

    return
