from settings import *
from report_settings import *
from common import *

from highton import Highton
from datetime import date, timedelta
import pytz
import traceback
import sys
import logging

# initialize
high = Highton(
    api_key=HIGHRISE_API_KEY,
    user=HIGHRISE_USER
)

local_tz = pytz.timezone(LOCAL_TZ)

logging.basicConfig(filename='error.log', level=logging.ERROR)

def reports_no_contacts_people():
    # no contacts report loop
    for rep in REPORTS_NC:
        cutoff = date.today() - timedelta(days=DAYS_BACK_P)

        # last activity before cutoff
        items = []

        for person in high.get_people():
            # person must have all tags but not the excluded tag
            ptags = [t.name for t in person.tags]
            if rep['alltags'].issubset(set(ptags)) and rep['excludetag'] not in ptags:
                latest = date(1970, 1, 1)
                # notes and emails
                activity = []
                if high.get_person_notes(person.highrise_id):
                    activity += high.get_person_notes(person.highrise_id)
                if high.get_person_emails(person.highrise_id):
                    activity += high.get_person_emails(person.highrise_id)
                if activity:
                    # Note: Highton retrieves dates in UTC (timezone-naive) format
                    latest = max(a.created_at for a in activity)
                    if latest.date() < cutoff:
                        latest_loc = pytz.utc.localize(latest).astimezone(local_tz)
                        items += [[person.highrise_id,
                                   PERSON_LINK.format(HIGHRISE_USER,
                                                      person.highrise_id,
                                                      person.first_name,
                                                      person.last_name),
                                   ', '.join([t.name for t in person.tags]),
                                   latest_loc.strftime('%m/%d/%Y'),
                                   (date.today()-latest_loc.date()).days]]

        items.sort(key=lambda k: k[4], reverse=True)
        items.insert(0, ['ID', 'Name', 'Tag(s)', 'Date of Last Contact', 'Days Since Last Contact'])
        subitems = [HTML_TR.format(''.join([HTML_TD.format(a) for a in item[1:]])) for item in items]

        send_report(HTML_TABLE.format(rep['title'], "".join(subitems)),
                    rep['title'],
                    EMAIL_FROM,
                    rep['to'],
                    rep['cc'])


def reports_no_assigned_people():
    # nobody assigned report loop
    for rep in REPORTS_NA:
        # persons
        items = []

        for person in high.get_people():
            # person with no coach tag assigned
            if not set(rep['notags']).intersection(set([t.name for t in person.tags])):
                items += [[person.highrise_id,
                           PERSON_LINK.format(HIGHRISE_USER, person.highrise_id,
                                              person.first_name, person.last_name),
                           ', '.join([t.name for t in person.tags])]]

        items.insert(0, ['ID', 'Name', 'Tag(s)'])
        subitems = [HTML_TR.format(''.join([HTML_TD.format(a) for a in item[1:]])) for item in items]

        send_report(HTML_TABLE.format(rep['title'], "".join(subitems)),
                    rep['title'],
                    EMAIL_FROM,
                    rep['to'],
                    rep['cc'])


def reports_no_contacts_cases():
    # no contacts (cases) report loop
    for rep in REPORTS_CNC:
        cutoff = date.today() - timedelta(days=DAYS_BACK_C)

        # cases
        items = []

        for case in high.get_cases():
            # cases not closed
            if case.closed_at is None:
                latest = date(1970, 1, 1)
                # notes and emails
                activity = []
                if high.get_case_notes(case.highrise_id):
                    activity += high.get_case_notes(case.highrise_id)
                if high.get_case_emails(case.highrise_id):
                    activity += high.get_case_emails(case.highrise_id)
                if activity:
                    # Note: Highton retrieves dates in UTC (timezone-naive) format
                    latest = max(a.created_at for a in activity)
                    if latest.date() < cutoff:
                        latest_loc = pytz.utc.localize(latest).astimezone(local_tz)
                        items += [[case.highrise_id,
                                  CASE_LINK.format(HIGHRISE_USER, case.highrise_id, case.name),
                                  latest_loc.strftime('%m/%d/%Y'),
                                  (date.today()-latest_loc.date()).days]]

        items.sort(key=lambda k: k[3], reverse=True)
        items.insert(0, ['ID', 'Name', 'Date of Last Contact', 'Days Since Last Contact'])
        subitems = [HTML_TR.format(''.join([HTML_TD.format(a) for a in item[1:]])) for item in items]

        send_report(HTML_TABLE.format(rep['title'], "".join(subitems)),
                    rep['title'],
                    EMAIL_FROM,
                    rep['to'],
                    rep['cc'])


def main():
    reports_no_contacts_people()
    reports_no_assigned_people()
    reports_no_contacts_cases()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        logging.exception("Error message")
