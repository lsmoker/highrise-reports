hrreports - virtualenv directory with python binaries and libraries
common.py - common modules for the app
report_settings.py - report-related settings
reports.py - the main program that runs the reports
settings.py - overall system settings
testmail.py - test script for emailing
